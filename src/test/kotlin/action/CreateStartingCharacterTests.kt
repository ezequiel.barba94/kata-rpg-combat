package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Characters
import oe.barbero.kata.rpgcombat.infrastructure.InMemoryCharacters
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CreateStartingCharacterTests {

    @Test
    fun `create a starting character`() {
        `when a starting character is created`()
        `then his starting health is`(STARTING_HEALTH)
        `and his level is`(STARTING_LEVEL)
        `and is alive`()
    }

    private fun `when a starting character is created`() {
        characterId = createStartingCharacter()
    }

    private fun `then his starting health is`(health: Int) {
        val character = characters.findById(characterId)
        assertThat(character.health).isEqualTo(health)
    }

    private fun `and his level is`(level: Int) {
        val character = characters.findById(characterId)
        assertThat(character.level).isEqualTo(level)
    }

    private fun `and is alive`() {
        val character = characters.findById(characterId)
        assertThat(character.isAlive).isTrue()
    }

    @BeforeEach
    fun beforeEach() {
        characterId = ""
        characters = InMemoryCharacters()
        createStartingCharacter = CreateStartingCharacter(characters)
    }

    private lateinit var characters: Characters
    private lateinit var createStartingCharacter: CreateStartingCharacter
    private lateinit var characterId: String

    companion object {
        private const val STARTING_HEALTH = 1000
        private const val STARTING_LEVEL = 1
    }
}