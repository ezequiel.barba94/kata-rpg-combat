package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Character
import oe.barbero.kata.rpgcombat.core.Characters
import oe.barbero.kata.rpgcombat.infrastructure.InMemoryCharacters
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class HealTests {
    @Test
    fun `a character can heal another`() {
        `given a character`()
        `and a wounded character`()
        `when the wounded is healed by`(HEALED_AMOUNT)
        `then his health is restored to`(WOUNDED_HEALTH + HEALED_AMOUNT)
    }

    @Test
    fun `healing cannot raise health over healing limit`() {
        `given a character`()
        `and a wounded character`()
        `when the wounded is healed by`(OVER_HEAL)
        `then his health is restored to`(MAX_HEALING)
    }

    private fun `given a character`() {
        val character = Character.createStarting()
        characters.put(character)
        healerId = character.id!!
    }

    private fun `and a wounded character`() {
        val character = Character.builder().apply {
            id = "victim"
            health = WOUNDED_HEALTH
            level = 1
        }.build()
        characters.put(character)
        woundedId = character.id!!
    }

    private fun `when the wounded is healed by`(healedAmount: Int) {
        heal(healerId, woundedId, healedAmount)
    }

    private fun `then his health is restored to`(health: Int) {
        val wounded = characters.findById(woundedId)
        assertThat(wounded.health).isEqualTo(health)
    }

    @BeforeEach
    fun beforeEach() {
        characters = InMemoryCharacters()
        heal = Heal(characters)
    }

    private lateinit var characters: Characters
    private lateinit var healerId: String
    private lateinit var woundedId: String
    private lateinit var heal: Heal

    companion object {
        private const val WOUNDED_HEALTH = 500
        private const val HEALED_AMOUNT = 100
        private const val OVER_HEAL = 600
        private const val MAX_HEALING = 1000
    }
}