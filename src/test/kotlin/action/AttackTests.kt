package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Character
import oe.barbero.kata.rpgcombat.core.Characters
import oe.barbero.kata.rpgcombat.infrastructure.InMemoryCharacters
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AttackTests {
    @Test
    fun `damage is subtracted from health`() {
        `given a character`()
        `and another character`()
        `when one attacks the other by`(DAMAGE_AMOUNT)
        `then the victim health drops to`(HEALTH - DAMAGE_AMOUNT)
    }

    @Test
    fun `character dies if health drops to zero`() {
        `given a character`()
        `and another character`()
        `when one attacks the other by`(FULL_LIFE_DAMAGE)
        `then the victim health drops to`(0)
        `and the victim dies`()
    }

    @Test
    fun `health cannot drop below zero`() {
        `given a character`()
        `and another character`()
        `when one attacks the other by`(FULL_LIFE_DAMAGE + DAMAGE_AMOUNT)
        `then the victim health drops to`(0)
        `and the victim dies`()
    }

    @Test
    fun `a character cannot hurt himself`() {
        `given a character`()
        `then a error occurs` {
            `when she tries to attack herself`()
        }
    }

    private fun `then a error occurs`(function: () -> Unit) {
        Assertions.assertThrows(SelfAttack::class.java, function)
    }

    private fun `when she tries to attack herself`() {
        attack(attackerId, attackerId, DAMAGE_AMOUNT)
    }

    private fun `given a character`() {
        val character = Character.createStarting()
        characters.put(character)
        attackerId = character.id!!
    }

    private fun `and another character`() {
        val character = Character.builder().apply {
            id = "victim"
            health = HEALTH
            level = 1
        }.build()
        characters.put(character)
        victimId = character.id!!
    }

    private fun `when one attacks the other by`(damage: Int) {
        attack(attackerId, victimId, damage)
    }

    private fun `then the victim health drops to`(health: Int) {
        val victim = characters.findById(victimId)
        assertThat(victim.health).isEqualTo(health)
    }

    private fun `and the victim dies`() {
        val victim = characters.findById(victimId)
        assertThat(victim.isAlive).isFalse()
    }

    @BeforeEach
    fun beforeEach() {
        characters = InMemoryCharacters()
        attack = Attack(characters)
    }

    private lateinit var characters: Characters
    private lateinit var attackerId: String
    private lateinit var victimId: String
    private lateinit var attack: Attack

    companion object {
        private const val HEALTH = 1000
        private const val DAMAGE_AMOUNT = 100
        private const val FULL_LIFE_DAMAGE = 1000
    }
}