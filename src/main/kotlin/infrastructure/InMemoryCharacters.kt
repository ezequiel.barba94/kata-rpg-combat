package oe.barbero.kata.rpgcombat.infrastructure

import net.jemzart.jsonkraken.get
import net.jemzart.jsonkraken.toJson
import net.jemzart.jsonkraken.toJsonString
import net.jemzart.jsonkraken.values.JsonObject
import oe.barbero.kata.rpgcombat.core.Character
import oe.barbero.kata.rpgcombat.core.Characters
import java.util.*

class InMemoryCharacters : Characters {
    private val characters = mutableMapOf<String, String>()

    override fun put(character: Character) {
        generateId(character)
        characters[character.id!!] = JsonObject(
            "level" to character.level,
            "health" to character.health,
            "maxHealth" to character.maxHealth
        ).toJsonString()
    }

    private fun generateId(character: Character) {
        character.id = character.id ?: UUID.randomUUID().toString()
    }

    override fun findById(characterId: String): Character {
        val json = characters[characterId]!!.toJson()
        return Character.builder().apply {
            id = characterId
            maxHealth = (json["maxHealth"] as Double).toInt()
            health = (json["health"] as Double).toInt()
            level = (json["level"] as Double).toInt()
        }.build()
    }

}
