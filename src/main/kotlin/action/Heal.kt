package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Characters

class Heal(private val characters: Characters) {
    operator fun invoke(healerId: String, woundedId: String, healedAmount: Int) {
        val healer = characters.findById(healerId)
        val wounded = characters.findById(woundedId)
        healer.heal(wounded, healedAmount)
        characters.put(wounded)
    }

}
