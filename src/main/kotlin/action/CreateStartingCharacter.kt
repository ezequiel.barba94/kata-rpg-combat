package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Characters
import oe.barbero.kata.rpgcombat.core.Character

class CreateStartingCharacter(private val characters: Characters) {
    operator fun invoke(): String {
        val startingCharacter = Character.createStarting()
        characters.put(startingCharacter)
        return startingCharacter.id!!
    }

}
