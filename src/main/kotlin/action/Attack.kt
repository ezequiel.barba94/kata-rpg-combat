package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Characters

class Attack(private val characters: Characters) {
    operator fun invoke(attackerId: String, victimId: String, damage: Int) {
        if (attackerId == victimId) throw SelfAttack()
        val attacker = characters.findById(attackerId)
        val victim = characters.findById(victimId)
        attacker.attack(victim, damage)
        characters.put(victim)
    }

}
