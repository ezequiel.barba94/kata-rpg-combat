package oe.barbero.kata.rpgcombat.action

import oe.barbero.kata.rpgcombat.core.Characters

class SelfHeal(private val characters: Characters) {
    operator fun invoke(characterId: String, healingAmount: Int) {
        val character = characters.findById(characterId)
        character.heal(character, healingAmount)
        characters.put(character)
    }

}
