package oe.barbero.kata.rpgcombat.core

interface Characters {
    fun put(character: Character)
    fun findById(characterId: String): Character
}
