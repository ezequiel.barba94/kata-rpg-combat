package oe.barbero.kata.rpgcombat.core

class Character constructor(
    private val hitPoints: HitPoints,
    private var _level: Int
) {
    var id: String? = null
    val maxHealth get() = hitPoints.maxHealth
    val health get() = hitPoints.remainingHP
    val level get() = _level
    val isAlive get() = health > 0

    fun attack(victim: Character, damage: Int) {
        victim.takeDamage(damage)
    }

    private fun takeDamage(damage: Int) {
        hitPoints.loss(damage)
    }

    fun heal(wounded: Character, healedAmount: Int) {
        wounded.restoreHealth(healedAmount)
    }

    private fun restoreHealth(health: Int) {
        hitPoints.restore(health)
    }

    companion object {
        fun createStarting() = Character(HitPoints(1000), 1)
        fun builder() = Builder()

        class Builder {
            var id: String? = null
            var maxHealth = 1000
            var health = maxHealth
            var level = 1
            fun build(): Character {
                val character = Character(HitPoints(maxHealth, health), level)
                character.id = id
                return character
            }
        }
    }
}