package oe.barbero.kata.rpgcombat.core

class HitPoints(
    val maxHealth: Int,
    private var _remainingHP: Int = maxHealth
) {
    val remainingHP get() = _remainingHP

    fun restore(health: Int) {
        val restoredHealth = remainingHP + health
        _remainingHP = if (restoredHealth > maxHealth) maxHealth else restoredHealth
    }

    fun loss(health: Int) {
        val lossHealth = remainingHP - health
        _remainingHP = if (lossHealth < 0) 0 else lossHealth
    }
}